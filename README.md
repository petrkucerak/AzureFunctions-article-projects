# Azure Functions příklady z článků

// TODO - Přidat odkazy na články, až budou vydaný

V každém projektu najdete složku _/template_, v této složce je ARM šablona, přes kterou můžete pro daný pojekt vytvořit resource na Azure.

## 1 - Úvod

Obsahuje kód k úvodnímu článku. Zapeneme přes příkaz func start. Kód je psaný v JavaScriptu (node v. 14.16.1)

## 2 - HTTP Trigger

Obsahuje kód k článku. Zapeneme přes příkaz func start. Kód je psaný v pythonu. Pokud chcete kód ozkoušet, tak zapněte funkci (func start). Poté zpusťte "python test-it.py".

## 3 - Scheduler

Obsahuje kód k úvodnímu článku. Zapeneme přes příkaz func start. Kód je psaný v JavaScriptu (node v. 14.16.1). Zapneme přes func start. Všechna nutná nastavení jsou napsána ve článku.
